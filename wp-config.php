<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_HOME', 'http://localhost:81/test/');
define('WP_SITEURL', 'http://localhost:81/test/');
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1:3306' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '14wwp@T12c~X}L M[<[3Z$DH-&:gWW5dkiggwy*cnY%$>|rBVKRD ,h]eaEy[sp,' );
define( 'SECURE_AUTH_KEY',  'rUX*(LdlA&ea#Rr6-cP6zw|6f?k?vE2&H[J9:m5u3=R+&Z|DM`te`~,J{=,dDD~3' );
define( 'LOGGED_IN_KEY',    'w);$ldd#ndPR[o1<T)b6cpt(u_zTnNfCY,_@X[v49/ wO+H|?Ek%Kivd<i!c*GQW' );
define( 'NONCE_KEY',        'Ba}O?4?[|9+|6g+~*[t@!|K2d~R%nM+<-s8M<V,X{{K<=K{`^<C8!N*QdQM+(T0o' );
define( 'AUTH_SALT',        'wEaMv8$7-@%5fh%aU<QHGq~+$K7Xj&n]3EI:F#C9TaG!5jn^K-8Ctocdxi58E]VM' );
define( 'SECURE_AUTH_SALT', 'iDSS/5]Mm1?a<a00kw8f(1XD4eEs(o10EH5}N&YWyo?lT8#G,N#q3#!-oKD]fMZ6' );
define( 'LOGGED_IN_SALT',   '>CITf1h]}fgRUWp>3f2{x|p|} m%*pXQW>QkU^SA$]vs752wU:Kn8Pr{fN}#?ChX' );
define( 'NONCE_SALT',       '6x|ljq_.V0YYdM 0cx!f`Ly?>n^0;-0n-<qA1[I&[!:0s3pRaZ8;H)K%y^7u%<(s' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
